# SPDX-FileCopyrightText: 2023 Federico Vaga <federico.vaga@vaga.pv.it>
#
# SPDX-License-Identifier: BSD-3-Clause

NAME=radioitaliastreaming
XSPF=$(NAME).xspf

all: web

validate:
	xmlstarlet validate -e -r http://www.xspf.org/validation/xspf-1_0.7.rng $(XSPF)

web:
	make -C $@

clean:
	make -C web $@

.PHONY: all web clean
