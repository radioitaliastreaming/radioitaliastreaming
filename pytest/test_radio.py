# SPDX-FileCopyrightText: 2023 Federico Vaga <federico.vaga@vaga.pv.it>
#
# SPDX-License-Identifier: BSD-3-Clause

import os
import xml.etree.ElementTree as ET
from urllib.parse import urlparse
from urllib.request import urlopen, Request
from urllib.error import URLError, HTTPError
import pytest


uagent = "Mozilla/5.0 (Linux; rv:102.0) Gecko/20100101 Firefox/102.0"
timeout_s = 10
ns = {"xspf": "http://xspf.org/ns/0/"}
xspf = ET.parse(os.path.join(".", "radioitaliastreaming.xspf"))
root = xspf.getroot()
tracks_top = root.iterfind("xspf:trackList/xspf:track", ns)


@pytest.fixture(scope="session")
def xspf_root():
    return root


@pytest.fixture()
def tracks(xspf_root):
    return xspf_root.iterfind("xspf:trackList/xspf:track", ns)


@pytest.fixture(scope="session", params=list(tracks_top))
def track(request):
    return request.param


class TestRadio():
    """
    Test per verificare la correttezza della lista. Verifica che tutti i
    collegamenti siano funzionanti. Assolutamente, non verifica che i flussi
    corrispondano davvero ai nomi. Spesso accade che il flusso non viene
    cancellato ma riassegnato ad un'altra stazione radiofonica.
    """

    def test_xml_validation(self):
        pass

    @pytest.mark.parametrize("tag,allow_empty", [("title", False),
                                                 ("info", True),
                                                 ("location", False),
                                                 ("image", True),
                                                 ("annotation", True)
                                                 ]
                             )
    def test_track_complete(self, track, tag, allow_empty):
        """
        Tutti i campi XSPF che usiamo sono presenti
        """
        elem = track.find("xspf:{}".format(tag), ns)
        assert elem is not None
        if not allow_empty:
            assert elem.text is not None

    def test_track_info_valid(self, track):
        """
        Verifica che il collegamento fornito nel campo "info" sia
        effettivamente raggiungibile.
        """
        info = track.find("xspf:info", ns)
        assert info is not None
        if info.text is None:
            pytest.skip("Non c'è un titolo")
        url = urlparse(info.text)

        assert all([url.scheme, url.netloc])
        assert url.scheme in ["http", "https"]

        try:
            req = Request(info.text, headers={"User-Agent": uagent})
            with urlopen(req, timeout=timeout_s) as web:
                assert web.getcode() == 200
        except Exception as err:
            assert False, "Can't open {}. {}".format(info.text, err)

    def test_location_valid(self, track):
        """
        Verifica che il collegamento fornito nel campo "location" sia
        effettivamente raggiungibile e che sia un flusso audio.
        """

        location = track.find("xspf:location", ns)
        assert location is not None
        assert location.text is not None
        url = urlparse(location.text)
        assert all([url.scheme, url.netloc])
        assert url.scheme in ["http", "https"]

        try:
            req = Request(location.text, headers={"User-Agent": uagent})
            with urlopen(req, timeout=timeout_s) as web:
                assert web.getcode() == 200
        except Exception as err:
            assert False, "Can't open {}. {}".format(location.text, err)

    def test_location_no_dup(self, track, tracks):
        """
        Non ci sono flussi duplicati
        """
        location_url = track.find("xspf:location", ns).text
        count = 0
        for tmp in tracks:
            tmp_url = tmp.find("xspf:location", ns).text
            if location_url == tmp_url:
                count += 1
        assert count == 1, "Trovato {} volte il flusso {}".format(count,
                                                                  location_url)
