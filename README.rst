.. SPDX-FileCopyrightText: 2023 Federico Vaga <federico.vaga@vaga.pv.it>
..
.. SPDX-License-Identifier: CC-BY-SA-4.0

.. image:: https://gitlab.com/radioitaliastreaming/radioitaliastreaming/badges/master/pipeline.svg
    :target: https://gitlab.com/radioitaliastreaming/radioitaliastreaming/
    :alt: stato elaborazione

Radio Italia Streaming
======================

.. note::
   The project is for Italian speakers, it does not make sense to write
   in English here

Il progetto è dedicato a coloro che vogliono ascoltare radio in lingua italiana
da un proprio riproduttore multimediale senza dover scaricare applicazioni o
siti web dedicati per ogni singola emittente.

Radio Italia Streaming non è altro che una lista di radio italiane coi loro
relativi flussi web. La lista è disponibile sul sito `Radio Italia Streaming`_
nei seguenti formati:

- `XSPF`_

Gli utenti possono scaricare questa lista nel formato più adatto al proprio
lettore multimediale.

Estensioni Disponibili
----------------------
Al momento sono disponibili le seguenti estensioni:

- `Amarok`_:  `Amarok Script`_
- `Kodi`_: `Kodi Addon`_

Per maggiori informazioni circa la loro installazione sui relativi riproduttori
multimediali, potete consultare il sito `Radio Italia Streaming`_ oppure
i rispettivi progetti.

.. _`XSPF`: https://www.xspf.org/
.. _`Radio Italia Streaming` : http://www.radioitaliastreaming.it/
.. _`Amarok` : https://amarok.kde.org/
.. _`Kodi`: https://kodi.tv/
.. _`Kodi Addon`: https://gitlab.com/radioitaliastreaming/ris-kodi-plugin
.. _`Amarok Script`: https://gitlab.com/radioitaliastreaming/ris-amarok-script
